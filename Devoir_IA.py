#!/usr/bin/env python
# coding: utf-8

# In[9]:


import pandas as pd
import numpy as np


# In[10]:


dataset=pd.read_csv('breastCancer.csv')
dataset.info()


# In[11]:


dataset.isnull().sum()


# In[12]:


dataset.replace('?',-9999,inplace=True)


# In[13]:



X = dataset.iloc[:,:-1]
y = dataset['class']


# In[14]:



from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.4, stratify=y)


# In[15]:


from sklearn.model_selection import KFold


# In[16]:


from sklearn.model_selection import GridSearchCV
from sklearn.neighbors import KNeighborsClassifier
grid_params={
    'n_neighbors':[3,5,7,9,11],
    'metric':['euclidean','manhattan']
}

gs=GridSearchCV(
    KNeighborsClassifier(),
    grid_params,
    verbose=1,
    cv=3,
    n_jobs=-1
)
gs_results=gs.fit(X_train, y_train)


# In[17]:


gs_results.best_params_


# In[18]:


from sklearn.neighbors import KNeighborsClassifier
knn= KNeighborsClassifier(n_neighbors=9,metric='euclidean')
knn.fit(X_train, y_train)
y_pred=knn.predict(X_test)


# In[19]:


from sklearn.metrics import  confusion_matrix
confusion_matrix(y_test, y_pred)


# In[ ]:





# In[ ]:




